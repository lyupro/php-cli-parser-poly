<?php

// Создание константы - адрес корневой папки
define('__DOCUMENT_ROOT__', dirname(__FILE__));

require_once __DOCUMENT_ROOT__."/bootstrap.php";

use App\Classes\ParseHtml;
use App\Classes\ParseLink;
use App\Classes\ParseImage;
use App\Classes\Saver;

/**
 * @param $argv
 * @param $mode
 * @return array
 */
function checkParams($argv, $mode){

    if(!isset($argv[2])){
        echo "Required second parameter: link=http... \n";
        echo $mode ." Stopped... (1) \n";
        exit();
    }

    $link = explode("=", $argv[2]);

    if($link[0] != 'link'){
        echo "Second parameter has to be: link=http... \n";
        echo $mode. " Stopped... (2) \n";
        exit();
    }

    return $link;
}

/**
 * @param $link
 * @param int $mode
 * @return array
 */
function parsingPages($link, $mode = 1){

    if($mode === 1){
        $link = $link[1];
    }

    // Парсим страницу по указанной ссылке
    $url = new ParseHtml($link);
    $html = ParseHtml::prepareSite($url::$_url);

    // Проверяем, запустился ли парсер за 1-м разом
    if($mode === 1){
        // Подготавливаем все ссылки на сайте в массив
        ParseLink::prepareLink();
        // Очищаем массив от лишней информации
        $links = ParseLink::clearLinks();
    }else{
        $links[] = $link;
    }

    // Подготавливаем все ссылки
    $images = ParseImage::prepareImage();

    new Saver(__DOCUMENT_ROOT__."/downloads/");
    Saver::saveToCSV($url::$domain."_Links.csv", $links);
    Saver::saveToCSV($url::$domain."_Images.csv", $images);

    echo "All links and images have been added to \n" .
        "'".__DOCUMENT_ROOT__."/downloads/'\n" ;

    return $links;
}

/**
 * @param $argv
 */
function parser($argv){

//    print_r($link); exit(); // DEBUG

    switch ($argv[1]){
        case "parse":
            echo "Parsing Started... \n";

            $link = checkParams($argv, "Parsing");

            $links = parsingPages($link);

            $command = readline("Do you want to continue? [y/n]\n");
            if($command === 'y'){
                echo "Good choice! Let's rock!\n";

                array_shift($links);

                foreach ($links as $link) {
                    parsingPages($link, 0);
                }
            } else {
                echo "Bye!\n";
                exit();
            }

            break;

        case "report":
            echo "Reporting Started... \n";

            $link = checkParams($argv, "Reporting");

            $domain = $link[1];

            Saver::getFromCSV($domain);

            break;
        case "help":
            echo "Available commands:\n" .
                "parse link=...\n" .
                "report link=...\n";

            sleep(1);

            echo "Required 'parse' parameters:\n" .
                "parse - starts parser\n" .
                "link=http... - link to site\n";

            sleep(1);

            echo "Required 'report' parameters:\n" .
                "report - starts parser\n" .
                "link=http... - link to site\n";

            sleep(1);

            break;
        default:
            echo "You have to enter required parameters!\n" .
                 "Example: php index.php parse link=http...\n" .
                 "For more details, please enter 'help'\n";
            break;
    }

}

parser($argv);