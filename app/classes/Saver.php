<?php

namespace App\Classes;

/**
 * Class Saver
 * @package App\Classes
 */
class Saver
{

    protected static $directory;

    /**
     * Saver constructor.
     * @param $path
     */
    public function __construct($path)
    {
        if(!is_dir($path)){
            mkdir($path, 0700);
            echo "Directory: '". $path ."' has been successfully created!";
        }
        $this::$directory = $path;
    }

    /**
     * @param $fileName
     * @param $array
     */
    public static function saveToCSV($fileName, $array)
    {
        try
        {
            if(is_array($array)){
                try
                {
                    $fo = fopen(self::$directory."".$fileName, "a+");
                    if( $fo )
                    {
                        foreach ($array as $link) {
                            $links[] = $link . "\n";
                        }

                        fputcsv($fo, $links);
                        fclose($fo);
                    }else{
                        throw new \Exception("\n".__METHOD__." | Can't open file!");
                    }
                } catch (\Exception $exception)
                {
                    echo $exception->getMessage();
                    exit();
                }

                echo "CSV file has been successfully created!\n";
            }else{
                throw new \Exception("\n".__METHOD__." | Second parameter isn't array!");
            }
        } catch (\Exception $exception)
        {
            echo $exception->getMessage();
            exit();
        }
    }

    /**
     * @param $domain
     */
    public static function getFromCSV($domain)
    {
        try
        {
            // Проверяем длину ссылку
            if (strlen($domain) >= 12) {
                $fullUrl = preg_match("#http#", $domain);
                if(!$fullUrl){
                    $findDomain = explode('/', $domain);
                    $domain = $findDomain[0];
                }else{
                    $findDomain = explode("/", $domain);
                    $domain = $findDomain[2];
                }

                self::checkCSV($domain);
            }else{
                throw new \Exception("\n".__METHOD__." | Minimum url length: 12 symbols!");
            }
        } catch (\Exception $exception)
        {
            echo $exception->getMessage();
            exit();
        }
    }

    /**
     * @param $domain
     */
    public static function checkCSV($domain)
    {
        $filesPath = __DOCUMENT_ROOT__ ."/downloads/";

        // Сканируем директорию
        $files = array_filter(array_values(array_diff(scandir($filesPath), array('.', '..'))));

        foreach ($files as $file) {

            preg_match_all("#[^".$domain."](.+)#", $file, $fileName);

            if($fileName[0][0] == '_Images.csv'){
                $echo = 'Images: ';
            }else{
                $echo = 'Links: ';
            }

            try
            {
                $fullPath = $filesPath ."". $file;
                if(file_exists($fullPath)){
                    try
                    {
                        $fo = fopen($fullPath, "r");
                        if($fo){
                            $getCSV = fgetcsv($fo);
                            fclose($fo);

                            echo "\n\nFounded $echo" . count($getCSV) .
                            "\nBy this link: \n" . $fullPath;
                        }else{
                            throw new \Exception("\n".__METHOD__." | Can't open file!");
                        }
                    } catch (\Exception $exception)
                    {
                        echo $exception->getMessage();
                        exit();
                    }
                }else{
                    throw new \Exception("\n".__METHOD__." | File '". $file ."' doesn't exist!");
                }

            } catch (\Exception $exception)
            {
                echo $exception->getMessage();
                exit();
            }
        }
    }
}