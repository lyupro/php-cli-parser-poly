<?php

namespace App\Classes;

/**
 * Class ParseHtml
 * @package App\Classes
 */
class ParseHtml
{
    public static $_url;
    public static $domain;

    protected static $html;

    /**
     * ParseHtml constructor.
     * @param string $link
     */
    public function __construct(string $link)
    {
        try
        {
            // Проверяем, пустая ли ссылка или нет
            if(!empty($link)){
                // Записываем переданную ссылку
                $this::$_url = $link;
            }else{
                throw new \Exception(__METHOD__." | Url is empty!");
            }
        } catch (\Exception $exception)
        {
            echo $exception->getMessage();
            exit();
        }

        try
        {
            if(isset($this::$_url)){
                return $this::$_url;
            }else{
                throw new \Exception(__METHOD__." | Empty url can not be set!");
            }
        } catch (\Exception $exception)
        {
            echo $exception->getMessage();
            exit();
        }
    }

    /**
     * @param string $url
     * @return bool|string
     */
    public static function prepareSite($url = "")
    {
        if(!$url){
            $url = self::$_url;
        }

        try
        {
            // Проверяем длину ссылку
            if (strlen($url) >= 12) {

                // Проверяем, передали нам полную ссылку или нет
                $fullUrl = preg_match("#http#", $url);

                if(!$fullUrl) {
                    $url = 'http://' . self::$_url;
                    self::$_url = $url;
                }

                $domain = explode("/", $url);

                self::$domain = $domain[2];

                $ch = curl_init($url);
                // Сохраняем ответ в переменную
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                // Следуем за редиректами
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                // Отключаем проверки, для загрузки страниц по https
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                self::$html = curl_exec($ch);

                curl_close($ch);

                return self::$html;
            }else{
                throw new \Exception("\n".__METHOD__." | Minimum url length: 12 symbols!");
            }
        } catch (\Exception $exception)
        {
            echo $exception->getMessage();
            exit();
        }
    }
}