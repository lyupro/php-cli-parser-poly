<?php

namespace App\Classes;

use App\Classes\ParseLink;

/**
 * Class ParseImage
 * @package App\Classes
 */
class ParseImage extends ParseLink
{
    protected static $images;

    /**
     * @return array
     */
    public static function prepareImage()
    {
        preg_match_all("#<img.+?src=\"(.+?)\".+?>#", self::$html, $result);

        foreach ($result[1] as $image) {
             $allImages[] = $image;
        }

        $allImages = array_unique($allImages);

        array_unshift($allImages, "Link: " . self::$_url . "\n");

        try
        {
            if( !empty($allImages) ){
                self::$images = $allImages;
            } else {
                throw new \Exception("\n".__METHOD__." | Images array is empty!" .
                "\nProblem with link: " . self::$_url);
            }
        } catch (\Exception $exception)
        {
            echo $exception->getMessage();
            exit();
        }

        return self::$images;
    }
}