<?php

namespace App\Classes;

use App\Classes\ParseHtml as Parser;

/**
 * Class ParseLink
 * @package App\Classes
 */
class ParseLink extends Parser
{
    protected static $links;

    /**
     * @return array
     */
    public static function prepareLink()
    {
        preg_match_all("#<a.+?href=\"(.+?)\".+?>#", self::$html, $result);

        foreach ($result[1] as $link) {
            $allLinks[] = $link;
        }

        $allLinks = array_unique($allLinks);

        array_unshift($allLinks, self::$_url, "Link: " . self::$_url . "\n");

        try
        {
            if( !empty($allLinks) ){
                self::$links = $allLinks;
            } else {
                throw new \Exception("\n".__METHOD__." | Links array is empty!");
            }
        } catch (\Exception $exception)
        {
            echo $exception->getMessage();
            exit();
        }

        return self::$links;
    }

    /**
     * @return mixed
     */
    public static function clearLinks()
    {
        if( is_array(self::$links)){
            foreach (self::$links as $link) {
                if(filter_var($link, FILTER_VALIDATE_URL)){
                    $clearLinks[] = $link;
                }
            }
        }

        self::$links = $clearLinks;

        return self::$links;
    }
}