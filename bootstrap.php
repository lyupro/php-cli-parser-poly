<?php

require_once __DOCUMENT_ROOT__."/app/config/MainConfig.php";

require_once __DOCUMENT_ROOT__."/vendor/autoload.php";

require_once __CLASSES__."ParseHtml.php";
require_once __CLASSES__."ParseLink.php";
require_once __CLASSES__."ParseImage.php";
require_once __CLASSES__."Saver.php";